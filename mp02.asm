; macros
;+++++++++++++++++++++

	;/**
	; *	display string on console
	; *	@param string, string length
	; **/
	%macro print 2
		mov eax, 4
		mov ebx, 1
		mov ecx, %1		;1st param = string
		mov edx, %2		;2nd param = string length
		int 0x80
	%endmacro

	;/**
	; *	prompt user for input
	; *	@param input variable
	; **/
	%macro prompt 1
		mov eax, 3
		mov ebx, 2
		mov ecx, %1		;where input is to be stored
		int 0x80
	%endmacro

;+++++++++++++++++++++

section .data
	input_msg db "Input ('e' to exit, append 'm' for mem.): "
	input_msg_len equ $-input_msg
	ty_msg db "Thanks for using. Bye!", 10
	ty_msg_len equ $-ty_msg
	err_msg db 'Math Error', 10
	err_msg_len equ $- err_msg
	new_line db 10
	br db "-----------", 10
	brLen equ $-br
	str: db "answer = %d", 10, 0
	stack: times 80 dd 0

section .bss
	exp resb 70
	expLen resd 1
	postfix resb 100
	postfixLen resd 1
	res resb 11
	temp resb 50
	tempLen resd 1
	stackSize resd 1
	curChar resb 1
	index resd 1
	itemTopVal resb 1
	cVal resb 1
	itemTop resb 1
	curVal resd 1
	ans resd 1
	num resd 1
	digits resd 1
	num2 resd 1
	num1 resd 1
section .text
	extern printf
	global main
main:
	mov ebp, esp
	mov [num], dword 0
;++++++++++++++++++++++++++++++++++++++++++

	while_input_not_e:
		; init variables
		call initVariables

		; prompt user to enter input
		print new_line, 1
		print input_msg, input_msg_len
		prompt exp

		; if input is 'e' then break
		mov al, [exp]
		mov bl, 'e'
		cmp al, bl
		je exit_program

		; convert exp to postfix
		conversion:
			; get the length of the input exp
			call getExpLen
			
			while_index_less_than_expLen:
				mov ecx, [index]
				mov ebx, [expLen]
				cmp ecx, ebx		; compare if index < expLen
				jge index_greater_than_equal_expLen	; index >= expLen
				; index < expLen
				; curChar = exp.charAt(index)
				mov al, [exp+ecx]	; exp.charAt(index)
				mov [curChar], al
;				print curChar, 1	; << temporary >>
;				print new_line, 1	; << temporary >>

				; if curChar is m then append previous answer
;				print res, 11
;				print new_line, 1
;				print br, brLen
				cmp [curChar], byte 'm'
				jne not_m
				mov esi, 0
				while_not_end_of_res:
					cmp esi, 11
					je skip_is_operator
;					movzx eax, byte [res+esi]
;					push eax
;					push str
;					call printf
;					add esp, 8
;					print br, brLen
;					mov al, [res+esi]
;					mov [itemTop], al
;					print itemTop, 1
;					print new_line, 1
;					print br, brLen
					mov al, [res+esi]
					cmp al, 0
					je inc_esi
					mov ecx, [postfixLen]
					cmp al, '-'
					jne not_neg
					mov [postfix+ecx], byte 'n'
					call inc_postfixLen
					jmp inc_esi
					not_neg:
					mov [postfix+ecx], al
					call inc_postfixLen
					inc_esi:
					inc esi
					jmp while_not_end_of_res

				; determine if curChar is operator or operand
				not_m:
				cmp [curChar], byte '0'
				jl is_operator
				; if (curChar >= '0')
				is_operand:
					; postfix += curChar + ""
					mov ecx, [postfixLen]
					mov al, [curChar]
					mov [postfix+ecx], al
					call inc_postfixLen
					jmp skip_is_operator

				; else if (curChar < '0')
				is_operator:
					call appendUnderscore
					; manageOperator(curChar)
					call manageOperator

				skip_is_operator:
					; index++
					call inc_index
					jmp while_index_less_than_expLen

			index_greater_than_equal_expLen:
				; appendUnderscore()
				call appendUnderscore
				; while (stack.isEmpty == false) {
				;	postfix += cStack.pop + "";
				; }
				xor ecx, ecx
				mov ecx, [tempLen]
				while_stack_is_not_empty:
					cmp ecx, 0
					je evaluation
					mov esi, [postfixLen]
					xor ax, ax
					dec ecx
					mov al, [temp+ecx]
					mov [postfix+esi], al
					call inc_postfixLen
					jmp while_stack_is_not_empty
 
		evaluation:
;			print postfix, 100;postfixLen		; << temporary >>
;			print new_line, 1
			call evaluate
			jmp while_input_not_e

		; print result
		print_result:
			
		; TODO: reinitialize all variables reuse them safely
		;jmp while_input_not_e

	call_err_msg:
	print err_msg, err_msg_len

	; exit program
	exit_program:
		call exit
	mov esp, ebp
;++++++++++++++++++++++++++++++++++++++++++

		exit:
	print new_line, 1
	print ty_msg, ty_msg_len

	mov eax, 1
	int 0x80
	ret

		getExpLen:
	mov ecx, 0
	while_not_end_of_expLen_count:
		mov al, [exp+ecx]	; get the current char in exp
		cmp al, 0		; compare curChar with '/u0000'
		je end_while_not_end_of_expLen_count
		inc ecx
		jmp while_not_end_of_expLen_count

		end_while_not_end_of_expLen_count:
	dec ecx				; decrease outlier
	;add ecx, '0'			; uncomment for printing expLen
	mov [expLen], ecx
	;print expLen, 1		; uncomment for printing expLen
	ret

		initVariables:
	mov [index], dword 0		; index = 0
	mov [expLen], dword 0		; expLen = 0
	mov [postfixLen], dword 0	; postfixLen = 0
	mov [tempLen], dword 0		; tempLen = 0
	mov [stackSize], dword 0	; stackSize = 0
	mov [tempLen], dword 0		; tempLen = 0
	mov [curChar], byte 0		; curChar = 0
	mov [itemTopVal], byte 0	; itemTopVal = 0
	mov [curVal], byte 0		; curVal = 0
	mov [ans], dword 0		; ans = 0
	mov [digits], dword 0		; digits = 0
	mov [num2], dword 0		; num2 = 0
	mov [num1], dword 0		; num1 = 0
	mov esi, 0
	l1:
		cmp esi, 70
		je end_l1
		mov [exp+esi], dword 0
		inc esi
		jmp l1
	end_l1:
;	mov esi, 0
;	l2:
;		cmp esi, 11
;		je end_l2
;		mov [res+esi], byte 0
;		inc esi
;		jmp l2
;	end_l2:
;	mov esi, 0
;	l3:
;		cmp esi, 50
;		je end_l3
;		mov [temp+esi], byte 0
;		jmp esi
;		jmp l3
;	end_l3:
	ret

		init_res:
	mov esi, 0
	l2:
		cmp esi, 11
		je end_l2
		mov [res+esi], byte 0
		inc esi
		jmp l2
	end_l2:
	ret

		inc_postfixLen:
	inc dword [postfixLen]
	ret

		inc_index:
	inc dword [index]
	ret

		inc_stackSize:
	inc dword [stackSize]
	ret

		dec_stackSize:
	dec dword [stackSize]
	ret

		inc_tempLen:
	inc dword [tempLen]
	ret

		dec_tempLen:
	dec dword [tempLen]
	ret

		appendUnderscore:
	; if (postfixLen > 0)
	cmp [postfixLen], dword 0
	jle end_appendUnderscore
	; if (postfix.charAt(postfixLen - 1) >= '0'
	;	&& postfix.charAt(postfixLen - 1) != '_')
	mov ecx, [postfixLen]
	dec ecx
	mov al, [postfix+ecx]
	cmp al, '0'
	jl end_appendUnderscore
	cmp al, '_'
	je end_appendUnderscore
	add ecx, 1
	mov [postfix+ecx], byte '_'
	call inc_postfixLen

		end_appendUnderscore:
	ret

		manageOperator:
	; if (curChar == '-')
	;	isNeg = manageNegativeOperator(c);
	mov al, [curChar]
	cmp al, '-'
	je manage_negative_operator
	jmp if_isNeg_equals_false
	
	manage_negative_operator:
		; if (index == 0)
		;	postfix += "n"
		; 	return true;
		cmp [index], dword 0
		jne check_on_greater_than_zero
		mov ecx, [postfixLen]
		mov [postfix+ecx], byte 'n'
		call inc_postfixLen
		jmp end_manageOperator

		check_on_greater_than_zero:
		; else if (index > 0 && exp.charAt(index - 1) < '0')
		;	postfix += "n"
		;	return true;
		cmp [index], dword 0
		jl if_isNeg_equals_false
		mov ecx, [index]
		dec ecx
		mov bl, [exp+ecx]
		cmp bl, '0'
		jge if_isNeg_equals_false
		mov ecx, [postfixLen]
		mov [postfix+ecx], byte 'n'
		call inc_postfixLen
		jmp end_manageOperator

	; if (isNeg == false)
	;	pushOperatorToStack(curChar)
	if_isNeg_equals_false:
		call pushOperatorToStack
	
	end_manageOperator:
	ret

		pushOperatorToStack:
	; if (curChar == '(') {
	;	stack.push(curChar)
	; } else if (curChar == ')') {
	;	while (stack.isEmpty() == false) {
	;		char item = stack.pop()
	;
	;		if (item != '(') {
	;			postfix += item + "(";
	;		else if (item == '(') {
	;			break;
	;		}
	;	}
	; } else if (curChar == '*' || curChar == '/'
	;	|| curChar == '+' || curChar == '-') {
	;	manageOperatorPrecedence();
	; }
	cmp [curChar], byte '('
	jne else_if_curChar_equals_close
		xor eax, eax
		xor ecx, ecx
		mov al, [curChar]
		mov ecx, [tempLen]
		mov [temp+ecx], al
		call inc_tempLen
		jmp end_pushOperatorToStack

	else_if_curChar_equals_close:
		cmp [curChar], byte ')'
		jne else_if_curChar_is_arith_op
		xor ecx, ecx
		mov ecx, [tempLen]
		while_stackSize_is_greater_than_zero:
			cmp ecx, 0
			je else_if_curChar_is_arith_op
			dec ecx
			call dec_tempLen
			xor eax, eax
			mov al, [temp+ecx]
			cmp al, '('
			je end_pushOperatorToStack
			mov esi, [postfixLen]
			mov [postfix+esi], al
			call inc_postfixLen
			jmp while_stackSize_is_greater_than_zero

	else_if_curChar_is_arith_op:
		call manageOperatorPrecedence
		jmp end_pushOperatorToStack

	end_pushOperatorToStack:
	ret

		manageOperatorPrecedence:
	; if (stack.isEmpty()) {
	;	stack.push(curChar);
	;	return;
	; }
	;
	; char itemTop = stack.pop();
	; int itemTopVal = ((itemTop=='*' || itemTop=='/')?(2):((itemTop=='(')?(0):(1)));
	; int cVal = ((c=='*' || c=='/')?(2):((c=='(')?(0):(1)));
	;
	; if (itemTopVal < cVal) {
	;	stack.push(itemTop);
	; } else if (itemTopVal >= cVal) {
	;	postfix += itempTop + "";
	; }
	;
	; stack.push(curChar);
	cmp [tempLen], dword 0
	jne tempLen_not_equals_zero;end_manageOperatorPrecedence;tempLen_not_equals_zero
	xor ecx, ecx
	mov ecx, [tempLen]
	xor eax, eax
	mov al, [curChar]
	mov [temp+ecx], al
	call inc_tempLen
	jmp end_manageOperatorPrecedence

	tempLen_not_equals_zero:
	xor eax, eax
	xor ecx, ecx
	call dec_tempLen
	mov ecx, [tempLen]
	mov al, [temp+ecx]
	mov [itemTop], al
	; int itemTopVal = ((itemTop='*' || itemTop=='/')?(2):((itemTop=='(')?(0):(1)));
	cmp [itemTop], byte '*'
	jne itemTop_not_mul
	mov [itemTopVal], byte 2
	jmp assign_cVal
	itemTop_not_mul:
	cmp [itemTop], byte '/'
	jne itemTop_not_div
	mov [itemTopVal], byte 2
	jmp assign_cVal
	itemTop_not_div:
	cmp [itemTop], byte '('
	jne itemTop_not_open
	mov [itemTopVal], byte 0
	jmp assign_cVal
	itemTop_not_open:
	mov [itemTopVal], byte 1
	; int cVal = ((c=='*' || c=='/')?(2):((c=='(')?(0):(1)));
	assign_cVal:
	cmp [curChar], byte '*'
	jne curChar_not_mul
	mov [cVal], byte 2
	jmp cmp_itemTopVal_and_cVal
	curChar_not_mul:
	cmp [curChar], byte '/'
	jne curChar_not_div
	mov [cVal], byte 2
	jmp cmp_itemTopVal_and_cVal
	curChar_not_div:
	cmp [curChar], byte '('
	jne curChar_not_open
	mov [cVal], byte 0
	jmp cmp_itemTopVal_and_cVal
	curChar_not_open:
	mov [cVal], byte 1

	cmp_itemTopVal_and_cVal:
	; if (itemTopVal < cVal) {
	;	stack.push(itemTop);
	; } else if (itemTop >= cVal) {
	;	postfix += itemTop + "";
	; }
	; stack.push(curChar);
;	print br, brLen			;<< temporary >>
;	add [itemTopVal], dword '0'	;^
;	print itemTopVal, 1		;^
;	sub [itemTopVal], dword '0'	;^
;	print new_line, 1		;^
;	add [cVal], dword '0'		;^
;	print cVal, 1			;^
;	sub [cVal], dword '0'		;^
;	print new_line, 1		;^
;	print br, brLen			;<< temporary >>
	xor eax, eax
	xor ecx, ecx
	mov al, [cVal]
	cmp [itemTopVal], al
	jge itemTopVal_greater_than_equal_cVal
	mov ecx, [tempLen]
	xor ebx, ebx
	mov bl, [itemTop]
	mov [temp+ecx], bl
	call inc_tempLen
	jmp push_curChar_to_stack

	itemTopVal_greater_than_equal_cVal:
	xor ecx, ecx	
	mov ecx, [postfixLen]
	xor eax, eax
	mov al, [itemTop]
	mov [postfix+ecx], al
	call inc_postfixLen
	
	;<<------ temporary ------->>
;	print br, brLen
;	print postfix, 100
;	print new_line, 1
;	print br, brLen
;	add [postfixLen], dword '0'
;	print postfixLen, 1
;	sub [postfixLen], dword '0'
;	print new_line, 1
;	print br, brLen
	;<<------------------------>>

	push_curChar_to_stack:
	xor ecx, ecx
	mov ecx, [tempLen]
	xor eax, eax
	mov al, [curChar]
	mov [temp+ecx], al
	call inc_tempLen

	;<<------- temporary ------->>
;	print br, brLen
;	print temp, 50
;	print new_line, 1
;	print br, brLen
;	add [tempLen], dword '0'
;	print tempLen, 1
;	sub [tempLen], dword '0'
;	print new_line, 1
;	print br, brLen
	;<<------------------------->>

	end_manageOperatorPrecedence:
	ret

		evaluate:
	; for (index = 0; index < postfixLen, index++) {
	;	curChar = postfix.charAt(index);
	;
	;	if (curChar == '_') {
	;		int num = convertStringToInt(temp);
	;		stack.push(num);
	;		temp = "";
	;	} else if (curChar >= '0') {
	;		temp += curChar + "";
	;	} else if (curChar < '0') {
	;		int num2 = stack.pop();
	;		int num1 = stack.pop();
	;
	;		ans = performOperation(num1, num2, curChar);
	;		stack.push(ans);
	;	}
	; }
	;
	; res = Integer.toString(ans);
	mov [index], dword 0
	mov [ans], dword 0
	mov [tempLen], dword 0
	mov [stackSize], dword 0

	while_index_less_than_postfixLen:
		xor eax, eax
		xor ebx, ebx
		xor ecx, ecx
		mov ecx, [index]
		mov ebx, [postfixLen]
		cmp ecx, ebx
		jge assign_ans_to_res
		mov al, [postfix+ecx]
		mov [curChar], al
;		print curChar, 1
;		print new_line, 1

		cmp [curChar], byte '_'
			jne else_if_curChar_greater_than_equals_zero
			; int num = convertStringToInt(temp);
			; stack.push(num);
			; temp = "";
			call convertStringToInt
			jmp inc_index_for_evaluate
		
		else_if_curChar_greater_than_equals_zero:
			cmp [curChar], byte '0'
			jl else_if_curChar_less_than_zero
			xor ecx, ecx
			mov ecx, [tempLen]
			xor eax, eax
			mov al, [curChar]
			mov [temp+ecx], al
			call inc_tempLen
			jmp inc_index_for_evaluate

		else_if_curChar_less_than_zero:
			call performOperation

		inc_index_for_evaluate:
			call inc_index
			jmp while_index_less_than_postfixLen

	assign_ans_to_res:	
		call convertIntToString

	end_evaluate:
	ret

		convertStringToInt:
	; int digits = 1;
	; int num = 0;
	;
	; for (int i = tempLen - 1; i >= 0; i--) {
	;	char curVal = temp.charAt(i);
	;
	;	if (curVal == 'n') {
	;		num *= -1;
	;	} else {
	;		num += (curVal-48) * digits;
	;		digits *= 10;
	;	}
	; }
	;
	; return num;
	; -----------------------------------------
	; int num = convertStringToInt(temp);
	; stack.push(num);
	; temp = "";
	
	;<<------- temporary ------->>
;	print br, brLen
;	print temp, 50
;	print new_line, 1
;	print br, brLen
;	add [tempLen], dword '0'
;	print tempLen, 1
;	sub [tempLen], dword '0'
;	print new_line, 1
;	print br, brLen
	;<<------------------------->>
	mov [digits], dword 1
	mov [num], dword 0
	mov edi, [tempLen]

	while_tempLen_greater_than_equals_zero:
		cmp edi, 0
		je push_num_to_stack;end_convertStringToInt;push_num_to_stack
		dec edi
		xor eax, eax
		mov al, [temp+edi]
		mov [curVal], eax
		;<<------- temporary ------->>
;		print br, brLen
;		print curVal, 1
;		print new_line, 1
;		print br, brLen
		;<<------------------------->>
		cmp [curVal], byte 'n'
		jne else_if_curVal_not_n;while_tempLen_greater_than_equals_zero;else_if_curVal_not_n
		neg dword [num]
		jmp push_num_to_stack

		else_if_curVal_not_n:
			sub [curVal], dword '0'
			xor eax, eax
			mov eax, [curVal]
			xor ebx, ebx
			mov ebx, [digits]
			mul ebx
			mov ebx, [num]
			add ebx, eax
			mov [num], ebx
			mov eax, [digits]
			mov ebx, 10
			mul ebx
			mov [digits], eax
			;<<------- temporary ------->>
;			print br, brLen
;			print curVal, 1
;			print new_line, 1
;			print digits, 1
;			print br, brLen
;			mov [digits], dword 1
;			print br, brLen
			;<<------------------------->>
			jmp while_tempLen_greater_than_equals_zero

	push_num_to_stack:
	xor eax, eax
	mov eax, [num]
	xor ecx, ecx
	mov ecx, [stackSize]
	mov [stack+ecx*4], eax
	call inc_stackSize
	mov [tempLen], dword 0
;	push eax
;	push dword str
;	call printf
;	add esp, 8

	end_convertStringToInt:
	;<<------- temporary ------->>
;	mov ecx, [stackSize]
;	dec ecx
;	mov ebx, [stack+ecx]
;	add ebx, 36
;	mov [num], ebx
;	print br, brLen
;	print num, 1
;	print new_line, 1
;	print br, brLen
	;<<------------------------->>
	ret

		performOperation:
	; int num2 = stack.pop();
	; int num1 = stack.pop();
	;
	; num = performOperation(num1, num2, curChar);
	; stack.push(ans);

	;<<---------temporary---------->>
;	add [stackSize], dword '0'
;	print stackSize, 1
;	sub [stackSize], dword '0'
;	print br, brLen
;	mov esi, 0
;	l1:
;		cmp esi, 20
;		je end_l1
;		push dword [stack+esi]
;		push dword str
;		call printf
;		add esp, 8
;		inc esi
;		jmp l1
;	end_l1:
;	print br, brLen
	;<<----------------------------->>

	xor ecx, ecx
	mov ecx, [stackSize]
	dec ecx
	call dec_stackSize
	xor ebx, ebx
	mov ebx, [stack+ecx*4]
	dec ecx
	call dec_stackSize
	xor eax, eax
	mov eax, [stack+ecx*4]
	
	addition:
	cmp [curChar], byte '+'
	jne subtraction
	add eax, ebx
	mov [num], eax
;	push eax
;	push dword str
;	call printf
;	add esp, 8
	jmp end_performOperation

	subtraction:
	cmp [curChar], byte '-'
	jne multiplication
	sub eax, ebx
	mov [num], eax
;	push eax
;	push dword str
;	call printf
;	add esp, 8
	jmp end_performOperation

	multiplication:
	cmp [curChar], byte '*'
	jne division
	imul eax, ebx
	mov [num], eax
;	push eax
;	push dword str
;	call printf
;	add esp, 8
	jmp end_performOperation

	division:
	cmp [curChar], byte '/'
	jne end_performOperation
	cmp ebx, 0
	je call_err_msg
	xor edx, edx
	idiv ebx
	mov [num], eax
;	push eax
;	push dword str
;	call printf
;	add esp, 8
	jmp end_performOperation

	end_performOperation:
	mov ecx, [stackSize]
	mov eax, [num]
	mov [stack+ecx*4], eax
	call inc_stackSize
	ret

		convertIntToString:
	call init_res
	cmp [num], dword 0
	jge num_greater_than_zero
	mov [res], dword '-'
	neg dword [num]

	num_greater_than_zero:
	xor ecx, ecx
	mov ecx, 10
	xor eax, eax
	mov eax, [num]
	mov ebx, 10

	while_eax_not_equals_zero:
	xor edx, edx
	xor ebx, ebx
	mov ebx, 10
	div ebx
	or edx, 30h
	mov [res+ecx], dl
	dec ecx
	cmp eax, 0
	jne while_eax_not_equals_zero

	end_convertIntToString:
	print res, 11
	ret
